﻿using System;
using System.Runtime.InteropServices;

namespace Hawk.Native.SOILCPP {
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 4)]
    internal struct NativeImage{
        internal int width;
        internal int height;
        internal int stride;
        internal int bpp;
        internal IntPtr data;
    }

    public class SOILImage : IDisposable{
        internal SOILImage(NativeImage img, IntPtr nativeImgPtr){
            nativeImg = img;
            this.nativeImgPtr = nativeImgPtr;
        }

        private SOILImage(){}
        internal NativeImage nativeImg;
        internal IntPtr nativeImgPtr;

        public int Width{ get { return nativeImg.width; }}
        public int Height { get { return nativeImg.height; }}
        public int Stride { get { return nativeImg.stride; }}
        public int BytePerPixel { get { return nativeImg.bpp; }}

        public IntPtr ImageData { get { return nativeImg.data; }}
        public int ImageDataSize { get { return Stride * Height; } }

        public void Dispose(){
            if(nativeImgPtr != IntPtr.Zero){
                SOIL.SharpSOIL_UnloadImage(nativeImgPtr);
                nativeImgPtr = IntPtr.Zero;
            }
        }

        public static SOILImage LoadBytes(byte[] data, bool force_rgba = false){
            return LoadData(Marshal.UnsafeAddrOfPinnedArrayElement(data, 0), data.Length, force_rgba);
        }

        public static SOILImage LoadData(IntPtr data, int size, bool force_rgba = false){
            var limage = SOIL.SharpSOIL_LoadImage(data, size, force_rgba);
            if (limage == IntPtr.Zero)
                return null;

            return new SOILImage(Marshal.PtrToStructure<NativeImage>(limage), limage);
        }

        public static SOILImage LoadFile(string fileName, bool force_rgba = false){
            var limage = SOIL.SharpSOIL_LoadFile(fileName, force_rgba);
            if (limage == IntPtr.Zero)
                return null;

            return new SOILImage(Marshal.PtrToStructure<NativeImage>(limage), limage);
        }
    }

    internal static class SOIL{
        [DllImport("libSOIL")]
        internal extern static IntPtr SharpSOIL_LoadImage(IntPtr data, int size, bool force_rgba);

        [DllImport("libSOIL")]
        internal extern static IntPtr SharpSOIL_LoadFile(string fileName, bool force_rgba);

        [DllImport("libSOIL")]
        internal extern static void SharpSOIL_UnloadImage(IntPtr image);
    }
}
