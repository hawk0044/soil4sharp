# SOIL For C# #

Simple static/shared library wrapper over SOIL (originally https://www.lonesock.net/soil.html) for using with C#

### Features ###
* CLion 2018 project
* OpenGL usage definition (in Makefile)
* Handy C# wrapper

### Extern "C" functions ###
#### There is no any exceptions, if image is unable to load then functions will return zero ####

` limage_t *SharpSOIL_LoadImage(void *data, int size, bool force_rgba) `
* loads an image from memory

` limage_t *SharpSOIL_LoadFile(const char *name, bool force_rgba) `
* loads an image from file

` void SharpSOIL_UnloadImage(limage_t *image) `
* Freeing up allocated image buffer and limage_t itself

### C# Wrapper functions ###
#### There is no any exceptions, if image is unable to load then functions will return zero ####
`SOILImage SOILImage::LoadBytes(byte[] data, bool force_rgba = false) `
* loads an image from bytes array

` SOILImage SOILImage::LoadData(IntPtr data, int size, bool force_rgba = false) `
* loads an image from bytes array located from ptr data with given size

` SOILImage SOILImage::LoadFile(string fileName, bool force_rgba = false) `
* loads an image from file
