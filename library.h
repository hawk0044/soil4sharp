#ifndef SOIL_LIBRARY_H
#define SOIL_LIBRARY_H

#include "platform_defs.h"

#pragma pack(push, 4)
typedef struct limage{
    int width;
    int height;
    int stride;
    int bpp;
    byte *data;
}limage_t, *limage_ptr;
#pragma pack(pop)

extern "C"{
UNITY_INTERFACE_EXPORT void UNITY_INTERFACE_API SharpSOIL_UnloadImage(limage_t *img);
UNITY_INTERFACE_EXPORT limage_ptr UNITY_INTERFACE_API SharpSOIL_LoadImage(const byte *bytes, int file_size, bool force_rgba);
UNITY_INTERFACE_EXPORT limage_ptr UNITY_INTERFACE_API SharpSOIL_LoadImageFile(const char *file_name, bool force_rgba);
}

#endif