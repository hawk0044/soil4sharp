//
// Created by Alexander on 16/04/2018.
//

#ifndef SOIL_PLATFORM_DEFS_H
#define SOIL_PLATFORM_DEFS_H

#if defined(__CYGWIN32__)
#define WINDOWS
#elif defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(_WIN64) || defined(WINAPI_FAMILY)
#define WINDOWS
#elif defined(__MACH__) || defined(__ANDROID__) || defined(__linux__) || defined(__QNX__)
#define UNIX
#else
#error Platform not supported
#endif

#if defined(WINDOWS)
#include <cstdlib>
#endif

#if defined(WINDOWS)
    #define UNITY_INTERFACE_API __stdcall
    #define UNITY_INTERFACE_EXPORT __declspec(dllexport)
#elif defined(UNIX)
#define UNITY_INTERFACE_API
#define UNITY_INTERFACE_EXPORT
#endif

typedef unsigned char byte;
#endif //SOIL_PLATFORM_DEFS_H
