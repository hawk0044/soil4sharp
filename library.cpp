#include "library.h"

#include "SOIL.h"
#include <memory>

extern "C"{
UNITY_INTERFACE_EXPORT limage_ptr UNITY_INTERFACE_API SharpSOIL_LoadImage(const byte *bytes, int file_size, bool force_rgba){
        if(bytes == nullptr || file_size <= 0)
            return nullptr;

        int w = 0;
        int h = 0;
        int c = 0;

        auto image = SOIL_load_image_from_memory(bytes, file_size, &w, &h, &c, force_rgba ? SOIL_LOAD_RGBA : SOIL_LOAD_AUTO);
        if(image == nullptr)
            return nullptr;

        if(force_rgba){
            c = 4;
        }

        auto ret_image = (limage_t*)malloc(sizeof(limage_t));
        memset(ret_image,0,sizeof(limage_t));

        ret_image->width = w;
        ret_image->height = h;
        ret_image->bpp = c * 8;
        ret_image->stride = w * c;
        ret_image->data = image;
        return ret_image;
    }

    UNITY_INTERFACE_EXPORT limage_ptr UNITY_INTERFACE_API SharpSOIL_LoadFile(const char *file_name, bool force_rgba){
        if(file_name == nullptr)
            return nullptr;

        int w = 0;
        int h = 0;
        int c = 0;

        auto image = SOIL_load_image(file_name, &w, &h, &c, force_rgba ? SOIL_LOAD_RGBA : SOIL_LOAD_AUTO);
        if(image == nullptr)
            return nullptr;

        if(force_rgba){
            c = 4;
        }

        auto ret_image = (limage_t*)malloc(sizeof(limage_t));
        memset(ret_image,0,sizeof(limage_t));

        ret_image->width = w;
        ret_image->height = h;
        ret_image->bpp = c * 8;
        ret_image->stride = w * c;
        ret_image->data = image;
        return ret_image;
    }

    UNITY_INTERFACE_EXPORT void UNITY_INTERFACE_API SharpSOIL_UnloadImage(limage_t *img){
        if(img == nullptr)
            return;

        if(img->data != nullptr){
            free(img->data);
            img->data = nullptr;
        }

        free(img);
    }
}